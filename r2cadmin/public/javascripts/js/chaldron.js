Chaldron = window.Chaldron || {};
PNG = Chaldron; // shortcut
SG = Chaldron;  // shortercut
C = Chaldron;   // shortestcut

C.ready = new $.Deferred(); // resolved after HTML Imports complete

if (!(window.console && console.log))
{	// PNG-2090: IE9 rendering of dynamic content
	console = // create dummy object when console is undefined
	{
		log: function(){},
		debug: function(){},
		info: function(){},
		warn: function(){},
		error: function(){}
	};
}

//PNG-2810 - Modify X-Editable button and datepicker templates
$.fn.editableform.buttons = '<div class="btn-group font-size-null"><button type="submit" class="btn btn-mini sg-color-saved editable-submit"><i class="iconsg-w-sgSave"></i></button><button type="button" class="btn btn-mini editable-cancel"><i class="iconsg-b-sgClose close"></i></button></div>';
$.fn.editabletypes.datefield.defaults.tpl = '<div class="input-append date"><input type="text"/><span class="add-on"><i class="iconsg-b-sgDate"></i></span></div>';
$.fn.datepicker.DPGlobal.headTemplate = '<thead><tr><th class="prev"><i class="iconsg-b-sgArrowLeft"/></th><th colspan="5" class="datepicker-switch"></th><th class="next"><i class="iconsg-b-sgArrowRight"/></th></tr></thead>';
$.fn.datepicker.DPGlobal.contTemplate = '<tbody><tr><td colspan="7"></td></tr></tbody>';
$.fn.datepicker.DPGlobal.footTemplate = '<tfoot><tr><th colspan="7" class="today"></th></tr><tr><th colspan="7" class="clear"></th></tr></tfoot>';

//////////////////////////////////////////////////////////////////////////////
// Chaldron.namespace Function  (aka 'ns')
//
// Adds a namespace object onto the Chaldron.
// To create Chaldron.your.namespace.here as nested objects
//    Chaldron.namespace("your.namespace.here");
//
// Dots in the input string cause `namespace` to create nested objects for
// each token. If any part of the requested namespace already exists, the
// current object will be left in place.  This allows multiple calls to
// `namespace` to preserve existing namespaced properties.
//
// Be careful with namespace tokens. Reserved words may work in some browsers
// and not others. For instance, the following will fail in some browsers
// because the supported version of JavaScript reserves the word "long":
//     Chaldron.namespace("really.long.nested.namespace");

(	function()
	{
		C.namespace = function(ns)
		{
			var c = C, d = ns.split('.');
                
			for (var j = 0; j < d.length; c = c[d[j++]])
				c[d[j]] = c[d[j]] || {};

			return c; // return the created namespace
		}

		C.ns = C.namespace; // shortcut
	}
)();


//////////////////////////////////////////////////////////////////////////////
// Chaldron.locateResource Function
//
// locateResource constructs a resourceURL given the desired resourceID
// to fetch and any DOM element inside the portlet.  This function will
// determine the PortletID by traversing the DOM element's ancestry.
//
// Note: do not invoke this function before the DOM is ready.
//
// The third 'args' argument is for adding additional parameters to the
// url.  If args is a string, then the string is added as a parameter
// called 'verb'.  If args is an object, then all properties in the object
// are added as parameters.

// To produce a resourceURL for a portlet containing a '#wizard' element
// that will retrieve a testplan with id=4 using a ReST service:
//   var url = Chaldron.locateResource('testplans/4', '#wizard');
// or
//   var url = Chaldron.locateResource('testplans/4', $('#wizard'));
// or
//   var url = Chaldron.locateResource('testplans/4', $('#wizard'), 'GET');
//

(	function($, me)
	{
		if (window.Liferay)
			AUI().use('liferay-portlet-url', 'liferay-session', function()
			{
				var last_timestamp = new Date().getTime();
				
				me.locateResource = function(id, el, args)
				{
					if (Liferay.Session)
					{	// extend session, but not more than once per minute
						var now = new Date().getTime();
						
						if (last_timestamp < now - 60000)
						{
							Liferay.Session.extend();
							last_timestamp = now;
						}
					}
					
					if ($.type(el) === 'string')
						el = $(el); // el can be a DOM object or selector
					
					var idAttr = el.closest('.portlet-boundary').attr('id');
					var portletId = idAttr.substring(7, idAttr.length - 1);
					var url = Liferay.PortletURL.createResourceURL();
					
					if (args && typeof args === 'string')
						url.setParameter('verb', args);
					
					if (args && typeof args === 'object')
						$.each(args, function(k, v) { url.setParameter(k, v); });
					
					// encodeURI(id) for handling the escape chars
					return url.setPortletId(portletId).setResourceId(encodeURI(id)).toString();
				}
			});
	}
)(jQuery, C);


//////////////////////////////////////////////////////////////////////////////
// Chaldron.setUser Function
//
// setUser creates a User namespace in Chaldron and populates it with
// attributes of the currently logged in user, as supplied by the calling
// function.  The calling function is expected to reside in the view.jsp,
// which has access to user information from the portletSession.  Unit-testing
// and debugging code could provide dummy data if necessary.
//

(	function($, me)
	{
		me.ns('User');
		me.setUser = function(email, id, role, employerName, employerID, count, customerName, customerID, customerOracleID, guid)
		{
			me.User.email = email;
			me.User.id = id;
			me.User.role = role;
			me.User.employerName = employerName;
			me.User.employerID = employerID;
			me.User.customerCount = count;
			me.User.customerName = customerName || employerName;
			me.User.customerID = customerID || employerID;
			me.User.customerOracleID = customerOracleID;
			me.User.guid = guid;
			
			me.User.isInternal = employerName == 'SungardAS';
			me.User.isExternal = !me.User.isInternal;
		}
	}
)(jQuery, C);


//////////////////////////////////////////////////////////////////////////////
// Chaldron.wait Function
//
// The Chaldron.wait() resolves creates a promise object and resolves it in
// the given number of milliseconds (msec defaults to 10 if falsy).
//
// Usage: C.wait(4000).then(someFunction);
//

(	function($, me)
	{
		me.wait = function(msec)
		{
			var deferred = $.Deferred();
			setTimeout(deferred.resolve, msec || 10);
			return deferred.promise();
		}
		
		me.yield = function()
		{	// yield to other event in queue
			return me.wait();
		}
	}
)(jQuery, C);


//////////////////////////////////////////////////////////////////////////////
// Chaldron.fadeInWebapp function
//
// Moves fixed-dashboard content from the webapp, using the selector
// [data-dashboard-content], into the thematic #fixed-dashboard element
//
// Fades in the page, including themeatic elements and the application's
// main element, which should have opacity: 0.
//

(	function($, me)
	{
		me.fadeInWebapp = function(mainElement)
		{
			// Move [data-dashboard-content] elements from webapp
			// markup into the thematic #fixed-dashboard element
			$('[data-dashboard-content]').each
			(	function()
				{
					$(this).appendTo('#app-dashboard');
				}
			);

			// Fade in the page, including the given main element
			$('#fixed-dashboard').fadeTo('slow', 1);
			$('#png-customer-container').fadeTo('slow', 1);
			$(mainElement).fadeTo('slow', 1);
		}
	}
)(jQuery, C);

//////////////////////////////////////////////////////////////////////////////
// Chaldron.Say Module
//
// Provides functions to post simple dialogs to the user.  Dialogs use
// the Pines Notify library with custom styling.  Dialogs are either
// modal centered in the window, or temporary and stacked.
//

(	function($, me)
	{
		var say = me.ns('Say'); // declare Say namespace

		var overlay_html = '<div class="ui-widget-overlay modal-backdrop-toplevel" style="display: none"></div>';
		var overlay = undefined;
		var modalDlg = undefined;
		var tempDlg = undefined;
		var yesno = undefined;
		
        var spinner_opts =
        {	lines: 12,// The number of lines to draw
            length: 4, // The length of each line
            width: 12, // The line thickness
            radius: 8, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            color: '#000', // #rgb or #rrggbb
            speed: 1.5, // Rounds per second
            trail: 50, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: 'auto', // Top position relative to parent in px
            left: 'auto', // Left position relative to parent in px
            visibility: true
        };
        
        var spinner = new Spinner(spinner_opts).spin();            

	    var stack_bar_top =  // stacking options for pNotify
	    {	dir1 : "down",
	    	dir2 : "right",
	    	spacing1 : 0,
	    	spacing2 : 0,
	    	push : "top"
	    };

	    var modalBeforeOpen = function(pnotify)
		{	// position modal in center of screen and overlay mask
			var top = ($(window).height() / 2) - (pnotify.height() / 2);
			var left = ($(window).width() / 2) - (pnotify.width() / 2);
            pnotify.css({ top: top, left: left });
            
            if (!overlay)
            	overlay = $(overlay_html).appendTo('body');
            
            if (!pnotify.opts.nonblock)
            	overlay.fadeIn("fast");
		};
		
		var modalBeforeClose = function()
		{	// remove overlay on close
            overlay.fadeOut("fast");
            modalDlg = undefined; // once closed, cannot reuse
		};
		
		var tempBeforeClose = function()
		{	// once closed, cannot reuse
			tempDlg = undefined;
		};
		
		var notify = function(options)
		{	// wrapper for $.pnotify, overriding vendor default vales
			// if option.target exists and is visible, then it
			// is reused/replaced, else new pnotify is created
			var commonOptions =
			{	type: 'info',
				closer : true,
				closer_hover: false,
				sticker: false,
				sticker_hover: false,
				history: false,
				nonblock: true,
				mouse_reset: false
			};
			
			var $$ = $; // default context is jQuery
			var target = options? options.target : undefined;
			
			if (target && target.is && target.is(':visible'))
				$$ = target; // replace existing pnotify

			return $$.pnotify($.extend({}, commonOptions, options));
		};
		
		var modal = function(options)
		{	// wrapper for notify(), providing modal options
			var modalOptions =
			{	title: '&nbsp;', // ensure pnotify adds title, even empty
				before_open: modalBeforeOpen,
		        before_close: modalBeforeClose,
				width: '24em',
				hide: false,
				stack: false,
				nonblock: false,
				target: modalDlg
			};
			
			if (modalDlg) // margin-top may have been overridden, so reset
				modalDlg.find('.ui-pnotify-title').css('margin-top', '0px');
			
			modalDlg = notify($.extend({}, modalOptions, options));
			
			modalDlg.find('.ui-pnotify-title').addClass('font120')
				.css('border-bottom', '1px solid rgba(0, 0, 0, .1)')
				.css('padding-bottom', '6px')
				.css('margin-top', '0px')
				.css('margin-bottom', '12px')
				.css('text-align', 'center');
			
			return modalDlg;
		};

		var spinmodal = function(options)
		{	// wrapper for modal(), specialized to add a spinner
			var dlg = modal(options);
			
			dlg.find('.ui-pnotify-title').append(spinner.el)
				.css('margin-top', '45px');
			
			dlg.find('.ui-pnotify-title .spinner')
				.css('left', dlg.width() / 2 - 16)
				.css('bottom', '45px');
			
			return dlg;
		};
		
		var stepmodal = function(options)
		{	// wrapper for modal(), specialized to add resolve/reject buttons
			// html for buttons must be in options.resolve or options.reject
			var dlg = modal(options);
			var title = dlg.find('.ui-pnotify-title').html('');
			yesno = new $.Deferred();

			if (options.resolve)
				title.append($(options.resolve).click(yesno.resolve));
			
			if (options.reject)
				title.append($(options.reject).click(yesno.reject));
			
			return yesno.promise().always(closeModal);
		};
		
		var closeModal = function()
		{	// dismiss modal, if it exists
			if (modalDlg) 
				modalDlg.pnotify_remove();
			
			if (yesno && yesno.state() === 'pending')
				yesno.reject(); // means no
		};
		
		var temp = function(options)
		{	// wrapper for notify(), specialized for stacking
			// temporarily displayed messages
			var stackOptions = 
			{	before_close: tempBeforeClose,
				width: '100%',
				hide: true,
				delay: 2500,
				stack: stack_bar_top,
		        addclass: 'stack-bar-top',
		        target: tempDlg
			};	

			closeModal(); // new temp message will dismiss a modal
			return tempDlg = notify($.extend({}, stackOptions, options));
		};
		
		var fyi = function(msg, title)
		{	// wrapper for temp(), specialized for informational messages
			title = title? title : 'FYI...';
			msg = msg? msg : 'Your request is being processed.';
			return temp({ title : title, text : msg });
		};
		
		var imbusy = function(msg, title, promise)
		{	// wrapper for temp(), specialized for debouncing
			var title = title? title : "Please wait...";
			var text = msg? msg : 'Your most recent request is still being processed.';
			
			if (!promise || promise.state() === 'pending')
				return temp({ title : title, text : text });
		};
		
		var huzza = function(msg, title)
		{	// wrapper for temp(), specialized for a success message
			// also wraps modal(), when preceeded by modal progress dialog
			if (!msg && !title && $.type(msg) === 'boolean')
				return closeModal(); // quiet overload: C.say.huzza(false);
			
			title = title? title : 'Success!';
			
			if (!msg || $.type(msg) != 'string')
				msg = 'Your request was successfully processed.';
			
			var options = { title : title, text : msg, type : 'success' };
			
			if (!modalDlg || !modalDlg.is(':visible'))
				return temp(options);
			
			var tempModalOptions = { hide: true, delay: 2500, nonblock: true };
			overlay.fadeOut("fast");
			return modal($.extend(options, tempModalOptions));
		};
		
		var plzfix = function(msg, title)
		{	// wrapper for temp(), specialized for client-side validation msg
			title = title? title : '';
			msg = msg? msg : 'Try again';
			temp({ title : title, text : msg, type : 'error', delay : 5000 }); 
		};
		
		var oops = function(jqXHR, textStatus, errorThrown)
		{	// wrapper for modal(), specialized to be jQuery Ajax fail callback
			// also a wrapper for temp(), specialized for client-side
			// data-entry validation messages.
			var title = 'An unexpected error occurred';
			var msg = 'Try again.  If the problem persists, contact your SunGard representative.';
			
			if (jqXHR && $.type(jqXHR) === 'string')
				return plzfix(jqXHR); // client-side validation message

			if (!textStatus || $.type(textStatus) != 'string')
				textStatus = 'Error';
			
			textStatus = textStatus.charAt(0).toUpperCase() + textStatus.slice(1);

			if (jqXHR.responseText && jqXHR.responseText.length > 10 && jqXHR.responseText.length <= 140) {
				closeModal();
				return modal({ title: textStatus, text : jqXHR.responseText, type : 'error' });
			}
			
			// not a user-readable msg from server, so post generic error msg
			title = title.replace('error', textStatus);
			
			if (errorThrown && $.type(errorThrown) === 'string')
				msg = 'HTTP ' + jqXHR.status + ': ' + errorThrown + '<br>' + msg;
			
			return modal({ title : title, text : msg, type : 'error' });
		};

		var rusure = function(msg, title)
		{	// wrapper for modal(), specialized for a templated yes/no dialog
			// returns promise resolved on-click-yes and rejected on-click-no
			title = title? title : 'Are you sure?';
			var dlg = modal({ title : title, text : '<div></div>', closer : false });
			yesno = new $.Deferred();
			
			var fn = function(html)
			{
				dlg.find('.ui-pnotify-text > div').replaceWith(html);
				dlg.find('.btn.yes').click(yesno.resolve);
				dlg.find('.btn.no').click(yesno.reject);
			};

		    me.Assets.render(me.Assets.hbYesNo, { msg : msg }).then(fn);
		    return yesno.promise().always(closeModal);
		};
		
		var plzw8 = function(msg, title, promise, msec)
		{	// wrapper for spinmodal(), specialized for communicating with
			// server.  Dialog is shown after delay, if promise is still pending
			var fn = function()
			{
				var t = title? title : 'Please wait...';
				var m = msg? msg : 'Your request is in progress';
				
				if (!promise || promise.state() === 'pending')
					spinmodal({ title : t, text : m });
			};

			msec = $.type(msec) === 'undefined'? 200 : msec;
			setTimeout(fn, promise? msec : 0);
		};

		var loading = function(promise, msg, title, msec)
		{	// wrapper for plzw8(), specialized with auto-close
			if ($.type(msec) === 'undefined' && $.type(title) === 'number')
			{	// msec was given as third and final arg
				msec = title;
				title = undefined;
			}
			
			plzw8(msg, title, promise, msec);
			return promise.then(closeModal);
		};
		
		// public interface
		say.notify = notify;
		say.modal = modal;
		say.spinmodal = spinmodal;
		say.stepmodal = stepmodal;
		say.closeModal = closeModal;
		say.temp = temp;
		
		// specialized dialogs
		say.fyi = fyi; // temporary message
		say.huzza = huzza; // temporary success message
		say.imbusy = imbusy; // temporary message
		say.plzfix = plzfix; // temporary client-side error message
		say.oops = oops; // modal or temporary client-side message
		say.rusure = rusure; // modal yes/no with auto-close
		say.plzw8 = plzw8; // modal with delay
		say.loading = loading; // modal with delay with auto-close
		
		me.say = say; // 'Say' and 'say' both work to access this module
	}
)(jQuery, C);


//////////////////////////////////////////////////////////////////////////////
// LiferayNavItems Module
//
// Provides a client-side mechanism to access Liferay's nav_items collection,
// which represents web pages viewable by the currently logged-in user.  The
// code is closely coupled to the desired rendering of navigation icons in
// the toolDrawer.
// 
// See sg-header.vm for code that pushes nav_items.
// See sg-toolDrawer.vm for code that gets nav_items and adds icons.
//
// To add a new navigation item:
// 	Chaldron.LiferayNavItems.push({ id: id, href: href, currentPage: url });
//
// To get an existing navigation item:
// 	var item = Chaldron.LiferayNavItems.get(id);
//
// To add an icon to an existing navigation item:
//	Chaldron.LiferayNavItems.addIcon(id, text, iconCSS);
//

(	function($, me)
	{
		if (window.Liferay)
		{
			var ns = me.ns("LiferayNavItems");
			var data = [	{ id: 'home', text: 'Home', icon: 'iconsg-at-w-sgatHome' },
			            	{ id: 'documents', text: 'Documents', icon: 'iconsg-at-w-sgatDocRepository' },
			            	{ id: 'testplanning', text: 'Test Planning', icon: 'iconsg-at-w-sgatTestPlan' },
			            	{ id: 'ccr', text: 'CCR', icon: 'iconsg-at-w-sgatConfigure' },
			            	{ id: 'recoveryautomation', text: 'Recovery Automation', icon: 'iconsg-at-w-sgatAutomation' },
			            	{ id: 'r2c', text: 'Recover2Cloud', icon: 'iconsg-at-w-sgatR2C-SR', text2: 'Server Replication' },
			            	{ id: 'companymanagement', text: 'Company Management', icon: 'iconsg-at-w-sgatCustomerManage' },
			            	{ id: 'usermanagement', text: 'User Management', icon: 'iconsg-at-w-sgatUserManage' },
			            	{ id: 'myprofile', text: 'My Profile', icon: 'iconsg-at-w-sgatHome' },
			            	{ id: 'ticketing', text: 'Ticketing', icon: 'iconsg-at-w-sgatTicketing' },
			            	{ id: 'solutions', text: 'Solutions', icon: 'iconsg-at-w-sgatSolutions' }
						];
			
			$.each
			(	data,
				function(idx, item)
				{
					item.icon2 = item.icon.replace('iconsg-at-w-sgat', 'iconsg-ai-w-sgai');
					item.icon3 = item.icon.replace('iconsg-at-w-sgat', 'iconsg-lpt-t-sglpt');
				}
			);
			
			var get = function(id)
			{	// retrieve the item whose id matches the given id
				for (var i = 0; i < data.length; i++)
					if (data[i].id === id)
						return data[i];			
			}
	
			var getActive = function()
			{	// retrieve the icon for the active item
				for (var i = 0; i < data.length; i++)
					if (data[i].classes.match('active$'))
						return data[i];		
			}
	
			var setHref = function(id, href)
			{	// set the href property of the given item id, if present
				var item = get(id);
				
				if (item)
					item.href = href;
			}
			
			var setStyle = function(currentPage)
			{	// sets the styling for nav_items: active vs non-active
				var url = currentPage;
				var struts = url.indexOf("/-/");
				
				if (struts >= 0)
					url = url.substring(0, struts);
				
				var qs = url.indexOf("?");
				
				if (qs >= 0)
					url = url.substring(0, qs);

				for (var i = 0; i < data.length; i++)
				{
					if (data[i].href && url)
						if (data[i].href.match(url + '$'))
						{
							data[i].active = 'active';
							$('#chaldron-customer-app-icon').addClass(data[i].icon2);
							$('#chaldron-customer-app-text').text(data[i].text);
							
							if (data[i].text2)
							{
								$('#chaldron-customer-app-text2').text(data[i].text2);
								$('#chaldron-customer-app-text').removeClass('positionDown');
								$('#chaldron-customer-app-text2').removeClass('positionDown');
							}
						}
						else
							data[i].active = '';
				}
			}
			
			// public interface
			ns.get = get;
			ns.setHref = setHref;
			ns.setStyle = setStyle;
			ns.getActive = getActive;
		
			AUI().ready(function()
			{
				var myprofile = Chaldron.LiferayNavItems.get('myprofile');
				
				if (myprofile)
					$('#png-myprofile-link').attr('href', myprofile.href);
				
				var home = Chaldron.LiferayNavItems.get('home');
				
				if (home)
					$('#png-home-link').attr('href', home.href);
				
				$('#headerArt-Items').fadeTo('fast', 1);					
			});
		}
	}
)(jQuery, C);


//////////////////////////////////////////////////////////////////////////////
// Assets Module
//
// Provides deferred execution of tasks upon successful load of assets from
// server.  Provides a cache of assets to satisfy load-once use case.
// Provides immediate execution of tasks if asset is cached.  Logs unexpected
// errors to JS console.  Use the then() or done() methods of jQuery's Promise
// interface to provide a callback function.
//
// To get an asset from cache or from the server:
//    var promise = Chaldron.Assets.get(url);
//
// To fetch an asset from the server:
//    var promise = Chaldron.Assets.fetch(url);
//
// To render when template and data are ready, and then insert
// into the DOM:
//    var fn = function(html) { $('#mydiv').append(html); }
//    Chaldron.Assets.render(templateURL, dataURL).then(fn);
//

(	function($, me)
	{
		var assets = me.ns('Assets');
		var promise = [];
		
		var get = function(url)
		{	// get new or existing promise of an asset, with browser caching
			var opts = { timeout:30000 };
			return promise[url] || (promise[url] = $.ajax(url, opts).fail(me.say.oops));
		}

		var fetch = function(url)
		{	// get new promise of an asset, forcing round-trip to server
			var opts = { timeout:30000, cache:false };
			return promise[url] || (promise[url] = $.ajax(url, opts).fail(me.say.oops));
		}
		
		var compile = function(html)
		{	// compile the given 'html' template into an object
			if (html.indexOf('{{') >= 0 && html.indexOf('}}') >=0)
				return Handlebars.compile(html);
			
			// add support for more template rendering engines here...
			
			return _.template(html);
		}
		
		var render = function(templateURL, dataURL)
		{	// Renders data indicated by dataURL through the template
			// indicated by templateURL.  The dataURL parameter may be a
			// string URL, a Promise, a Backbone Model/Collection/View,
			// or an plain object containing the data to be rendered.
			if (!promise[templateURL]) // load and compile template once
			{
				promise[templateURL] = fetch(templateURL).then(compile);
				
				if (!dataURL)
					return promise[templateURL];
			}
			
			// helper fn renders data through template, returns string
			var fn = function(template, data) { return template(data); }
			
			// data is a Promise or a plain object
			var data = $.type(dataURL) === 'string'? fetch(dataURL) : dataURL;
			
			// ... or a Backbone Model/Collection with a toJSON() method
			data = data.toJSON? data.toJSON() : data;
			
			// ... or a Backbone View/Layout with a serialize() method
			data = data.serialize? data.serialize() : data;
			
			// add support for more MV* frameworks here...
			
			// return the promise of data fully rendered through template
			return $.when(promise[templateURL], data).then(fn);
		}

		/////////////////////////////////////////////////////////////////////
		// Declarative construction of page from lodash/handlebars templates
		// using data-* attributes.
		//
		// To load foo.html as a template with arbitrary parameters:
		// <div data-chaldron-import="foo.html" data-param1="flurbish"></div>
		//
		// The <div> DOM element will be replaced by the contents of foo.html,
		// which can be a template, and any data-* attributes in the <div>
		// are sent as parameters to the templating engine.
		var deferredImports = [];
		
		var processImport = function()
		{	// replace this DOM element with templated markup
			var el = $(this);
			var path = assets.templatesPath + el.data('chaldron-import');
			var replace = function(html) { el.replaceWith(html); }
			var dataset = {};
			$.each(el.data(), function(name, value) { dataset[name] = value; } );
			var deferred = render(path, dataset).then(replace);
			deferredImports.push(deferred);
		}
		
		var processImports = function()
		{	// process import declarations until none remain
			deferredImports = [];
			$("[data-chaldron-import]").each(processImport);
			
			if (deferredImports.length > 0) // more imports to process
				$.when.apply($, deferredImports).then(processImports);
			else if (C.ready.state() == 'pending')
				C.ready.resolve();
		}

		$(processImports);  // on DOM ready, process import declarations
		
		if (window.Liferay) // remove any prototype-specific markup
			C.ready.then
			(	function()
				{
					var el = $('[data-chaldron-prototype]');
					
					if (el.data['chaldron-prototype'] == 'remove')
						el.remove();
					else if (el.data['chaldron-prototype'] == 'table')
						$('[data-chaldron-prototype] > *').slice(1).remove();
					else
						el.empty();
				}
			);

		/////////////////////////////////////////////////////////////////////
		
		/////////////////////////////////////////////////////////////////////
		// public interface
		assets.get = get;
		assets.fetch = fetch;
		assets.render = render
		assets.handlebars = render;
		assets.underscore = render;
		assets.templatesPath = '/chaldron-theme/templates/';
		assets.hbButtonlist = '/chaldron-theme/templates/hb-buttonlist.html';
		assets.hbYesNo = '/chaldron-theme/templates/hb-dlg-yesno.html';
		
		if (!window.Liferay)
		{	// for Prototype UX website and local Jetty deployments
			assets.templatesPath = '/templates/';
			assets.hbButtonlist = '/templates/hb-buttonlist.html';
			assets.hbYesNo = '/templates/hb-dlg-yesno.html';
		}
	}
)(jQuery, C);


//////////////////////////////////////////////////////////////////////////////
// Popover Module
//
// Provides management of Bootstrap popovers.  Implements the Singleton
// design pattern, enforcing one open Bootstrap Popover at a time.
//
// To render a popover associated with a domElement using a handlebars
// template fetched asynchronously from the server:
//   Chaldron.PopoverManager.show(triggerElement);
//
// Notes:
// 1) Declarative configuration of Bootstrap Popover HTML...
//    The popover has outer-html.  This code allows the author
//    of the innner template content to declaratively configure
//    certain aspects of the outer-html.  For example, a popover template
//    from the test-planning-portlet might declaratively configure:
//      <div class="hide popover-config"
//        data-popover-class="border-color-application"
//        data-arrow-class="arrow-application"
//        data-title-class="hide"
//      ></div>
//
// 2) Ensure that the trigger element is wired to create the popover.  This
//	  example is a click handler for an ellipsis icon.  The 'model' may be
//	  a URL, a Promise, a Backbone Model/Collection, a Backbone View/Layout,
//    or a plain object containing data to render.
//		var el = this.$('.iconsg-b-sgEllipsis');
//		if (!el.data('model'))
//		{
//			el.data('model', new Application.Views.EllipsisPopover({model: this.model}));
//			el.data('model-template', app.templatesPath + "popovers/application_ellipsis.html");
//		}
//		C.PopoverManager.show(el);	
//
// 3) Clicking on the trigger element again will hide and destroy the popover.
//
// 4) When adding a close/dismiss button in popover content, declare onclick attr:
//      Chaldron.PopoverManager.hide(this);
//
// 5) The PopoverManager Module is aware of nested popovers.  The Singleton
//    design pattern is enforced at each level, so creating a child popover
//    does not close the parent popover, but will close an open sibling.
//
// 6) The show() method returns a Promise.  Chain a callback using jQuery's
//    then() method to perform post-rendering activities, such as wiring up
//    event handlers for nested popovers and other widgets inside the popover:
//      Chaldron.PopoverManager.show(el).then(doSomethingAfterRender);
//
(	function($, me)
	{
		var PopoverManager = me.ns('PopoverManager');
		var singleton = []; // clickable DOM element that triggers the popover
		
		var isVisible = function(el)
		{
			return $(el || this).next('.popover:visible').length > 0;
		}
		
		var hide = function(el)
		{	// el or this is a DOM element inside a popover
			$(el || this).closest('.popover').prev().off('shown.bs.popover').popover('destroy');
		}
		
		var onShown = function(e)
		{	// triggered by Bootstrap
			var d = $(e.currentTarget).parents('.popover').length; // depth

			if (singleton[d] && singleton[d][0] != e.currentTarget)
				singleton[d].off('shown.bs.popover').popover('destroy');

			// set singleton at depth=d to currently shown popover
			singleton[d] = $(e.currentTarget);
			
			// find a marked configuration element in templatized content
			var s = singleton[d].next('.popover');
			var c = s.find('.popover-config');

			// add classes according to 'data-' attributes, when present
			s.addClass(c.attr('data-popover-class'));
			s.find('.popover-title').addClass(c.attr('data-title-class'));
			s.find('.arrow').addClass(c.attr('data-arrow-class'));
			
			if (s.hasClass('bottom'))
			{
				var center = s.offset().left + s.width() / 2;
				var sign = center < $(window).width() / 3 ? '1'
							: center > $(window).width() / 3 * 2? '-1'
							: 0; // near center, so don't adjust arrow
							
				var delta = s.width() / 2.4 * sign;
				s.css({left: s.position().left + delta});
				s.find('.arrow').css({left: s.width() / 2 - delta});
			}
			
			if ($(window).height() < s.height())
				s.height(Math.max(0,$(window).height() - 50));
		}

		var create = function(html, el)
		{	// create, configure, and show the popover, given fully-rendered
			// html content and a DOM element that triggers the popover
			if ($(el).data('bs.popover'))
				return $(el).off('shown.bs.popover').popover('destroy');

			// create popover html and add it to the DOM
			var title = $(el).attr('data-popover-title');
			var width = $(el).attr('data-width');
			var template = '<div class="popover" style="width: auto; max-width: none; z-index: 2147483647"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>';

			if (width)
				if (width.substring(0,3) == 'pop') // .popover### class
					template = template.replace('"popover"', '"popover ' + width + '"');
				else // set width style
					template = template.replace('auto', width);
			
			var config = { title: title, template: template, content: html, html: true };
			return $(el).on('shown.bs.popover', onShown).popover(config).popover('show');
		}
		
		var show = function(el)
		{	// make the promise of a fully rendered Popover Singleton
			// el is the clickable element associated with the popover
			var template = el.data('model-template');
			var data = el.data('model');
			var promisedHtml = me.Assets.render(template, data);
			var popoverPromise = $.when(promisedHtml, el || this).then(create);
			
			if (data.render) // data is a Backbone View
				return popoverPromise.then
				(	function()
					{
						data.$el = $(el).next().find('.popover-content');
						data.render();
					}
				);
			
			return popoverPromise;
		}
		
		// public interface
		PopoverManager.hide = hide;
		PopoverManager.isVisible = isVisible;
		PopoverManager.create = create;
		PopoverManager.show = show;
		PopoverManager.hide = hide;
	}
)(jQuery, C);


//////////////////////////////////////////////////////////////////////////////
// ToolDrawer Module
//
// Defines the behavior for the top level navigation between applications.  Used
// by scripts in the sg-tooldrawer.vm file.
//
(	function($, me)
	{
		var tooldrawer = me.ns('tooldrawer');
		var isOpen = false;
		var ready = new $.Deferred();

		var addMenu = function()
		{
			var title = arguments[0];
			var buttons = Array.prototype.slice.call(arguments, 1);
			
			if (!buttons || buttons.length < 1)
				return;
			
			var items = { title: title, buttons: buttons };
			
			ready.then
			(	function(template)
				{
					var html = _.template(template, items);
					$('.png-AppDrawer-Buttons:first').prepend(html);
				}
			);
		}

		var clickCustomerName = function()
		{
			$('#chaldron-customer-name').toggle();
			$('#chaldron-customer-select').toggle();
		}
		
		tooldrawer.addMenu = addMenu;
		tooldrawer.clickCustomerName = clickCustomerName;
		tooldrawer.isOpen = function() { return isOpen; }
		
		me.toolDrawer = tooldrawer;
		me.ToolDrawer = tooldrawer;
		
		$(document).ready
		(	function()
			{
				var customerName = C.User.customerName || C.User.employerName || 'Unknown Customer';
				
				if (customerName.length > 16)
				{
					customerName = customerName.substring(0, 16) + "...";
					$('#chaldron-customer-name').attr('title', C.User.customerName);
				}
				
				$('#chaldron-customer-name').text(customerName);
				var template = $('#chaldron-navigation-template').html();
				ready.resolve(template);
			}
		);

	}
)(jQuery, C);


//////////////////////////////////////////////////////////////////////////////
// VideoDrawer Module
//
// Defines the behavior for the video drawer.  Used by scripts in the
// sg-videodrawer.vm file.
//
(	function($, me)
	{
		var videodrawer = me.ns('videodrawer');
		var isOpen = false;
		var el = '#chaldron-videodrawer';
		var firstOpen = new $.Deferred();
		var clickVideoDrawer = function()
		{
			if (isOpen)
			{
				isOpen = false;
				$(el).animate({ right: '-350px', opacity: 0 }, 'fast');
				stopAllVideos();
			}
			else
			{
				isOpen = true;
				$(el).animate({ right: '0px', opacity: 1 }, 'fast');
				firstOpen.resolve();
			}
		};

		var addVideo = function(id, src, title)
		{
			var video = { id: id, src: src, title: title };
			var template = $('#png-video-drawer-template').html();
			var html = _.template(template, video);
			$('#chaldron-videodrawer-toggle').show();
			firstOpen.then(function() { $('#video-accordion').append(html) });
		};

		var stopAllVideos = function()
		{
			var videos = $(el).find('iframe');
			
			for (var v = 0; v < videos.length; v++)
				if ($(videos[v]).is(':visible'))
				{
					var oldSrc = $(videos[v]).attr('src');
					$(videos[v]).attr('src', '').attr('src', oldSrc);
				}
		}

		videodrawer.addVideo = addVideo;
		videodrawer.stopAllVideos = stopAllVideos;
		videodrawer.isOpen = function() { return isOpen; }
		videodrawer.clickVideoDrawer = clickVideoDrawer;
		
		me.videoDrawer = videodrawer;
		me.VideoDrawer = videodrawer;
		
		$(document).ready
		(	function()
			{
				addVideo('PortalIntroVideo', '//player.vimeo.com/video/77729898', 'A Brief Tour of the Portal');
			}
		);
	}
)(jQuery, C);


