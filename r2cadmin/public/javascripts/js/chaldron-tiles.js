AUI().ready(
	'aui-template', 'aui-sortable', 'transition',
	function(A)
	{
		//////////////////////////////////////////////////////////////////////
		// private utility methods
		//
		
		"use strict"; // has JSLint seal of approval
		
		var // using variable assignment for functions to appease JSLint
		
		nameAttr = function(thing)
		{	// return the name of this thing
			if (!A.Lang.isUndefined(thing.name))
			{	// thing is object with name member
				return thing.name;
			}
			
			if (!A.Lang.isUndefined(thing.getAttribute))
			{	// thing is A.Node, so return ubiquitous title attribute
				return thing.getAttribute('title');
			}
			
			return thing.toString(); // thing is string or object
		},
	
		getMapping = function(region)
		{	// gets list of regions, templates, and initial items
			return A.one('.TilesMapping[title="' + nameAttr(region) + '"]');
		},
	
		getRegion = function(thing)
		{	// get the region node associated with this thing
			if (A.Lang.isString(thing))
			{
				if (thing.indexOf('<') >= 0)
				{	// thing is a string containing HTML markup
					return A.Node.create(thing);
				}
				
				// thing is string containing region name
				return A.one('.TilesRegion[title="' + thing + '"]');
			}
	
			if (!A.Lang.isUndefined(thing.region))
			{	// thing is an object with a region member
				return A.one('.TilesRegion[title="' + thing.region + '"]');
			}
	
			if (A.Lang.isUndefined(thing.type))
			{	// this thing is not an event
				return thing;
			}
			
			if (thing.type.substring(0,5) === 'drag:')
			{	// thing is a Tile drag/drop event
				return thing.target.get('node').get('parentNode');
			}
			
			// thing is a Tile-click event
			return thing.currentTarget.get('parentNode');
		},

		
		//////////////////////////////////////////////////////////////////////
		// Event Publications API
		//
		// Respond to click and move events by subscribing to TilesClicked
		// and TilesMoved events.
		//
		
		fireTilesClick = function(e)
		{	// publish click event
			Liferay.fire(
				'TilesClicked',
				{
					region: nameAttr(getRegion(e)),
					item: e.currentTarget.getData('item'),
					command: nameAttr(e.target)
				}
			);
			
			Liferay.Session.extend();
		},

		fireTilesMoved = function(e)
		{	// publish click event
			var tile = e.target.get('node'),
				region = getRegion(e);
			
			tile.setStyle('top', 0)
				.setStyle('left', 0)
				.setStyle('margin', getMapping(region).getStyle('margin'));

			Liferay.fire(
				'TilesMoved',
				{
					region: nameAttr(region),
					item: tile.getData('item'),
					command: nameAttr(e.target.get('node'))
				}
			);

			A.all(".TilesRegion").each(
				function(region)
				{	region.transition({	duration: 0.6,
										opacity: { value: 1.0 }
										});
				}
			);
			
			Liferay.Session.extend();
		},
		
		//////////////////////////////////////////////////////////////////////
		// private utility methods
		//
		
		nameEquals = function(thing)
		{	// callback sent to A.Array.find()
			return nameAttr(this) === nameAttr(thing);
		},
		
		emptyRegion = function(thing)
		{	// remove all tiles from region
			return getRegion(thing).empty();
		},
		
		renderTile = function(tile)
		{	// render the current tile using template and data in item
			var item = tile.getData('item'),
				region = tile.get('parentNode'),
				templates = region.getData('templates'),
				name = A.Array.find(templates, nameEquals, item.template),
				key = nameAttr(region) + '_' + name,
				template = A.ChaldronTemplate.get(key);
	
			if (A.Lang.isUndefined(template))
			{	// did not find template by key, so try by name
				template = A.ChaldronTemplate.get(name);
			}
			
			if (A.Lang.isNull(template))
			{	// region does not accept this template, so tile is not displayed
				return tile.hide();
			}

			// render item and markup into tile node and display inline-block
			template.render(item, tile);
			
			tile.setStyle('top', 0)
				.setStyle('left', 0)
				.setStyle('display', 'inline-block')
				.setStyle('margin', getMapping(region).getStyle('margin'))
				.show();
		},
		
		itemize = function(thing)
		{
			if (!A.Lang.isUndefined(thing.template))
			{	// thing is an object with template member
				return thing;
			}
			
			// thing is A.Node of unordered list containing item data
			var item = { template: nameAttr(thing.get('parentNode')) };
			thing.get('children').each(function(m) { item[nameAttr(m)] = m.html(); });
			return item;
		},
		
		addTile = function(thing)
		{	// create a new Tile (node), and append to 'this' region
			var region = this,
				item = itemize(thing),
				tile = A.Node.create('<div class="TilesTile"></div>');
			
			item.id = A.stamp(tile);
			region.append(tile.setData('item', item));
			renderTile(tile);
		},
	
		addTemplate = function(thing)
		{	// create and register a template from this thing
			var region = this,
				name = nameAttr(thing),
				key = nameAttr(region) + '_' + name,
				templates = region.getData('templates'),
				template = null;
			
			templates.push(name);
			region.setData('templates', templates);
			
			if (A.Lang.isUndefined(thing.getAttribute))
			{	// thing is string id of a template node
				A.ChaldronTemplate.register(key, A.ChaldronTemplate.from('#' + name));
				return;
			}

			if (thing.getAttribute('id'))		
			{	// thing is A.Node containing template
				A.ChaldronTemplate.register(thing.get('id'), thing.toTPL());
				return;
			}

			template = thing.one('div'); // thing is A.Node inside TilesMapping div
			
			if (A.Lang.isNull(template))
			{	// thing is A.Node whose name is dom id of a template node
				A.ChaldronTemplate.register(name, A.ChaldronTemplate.from('#' + name));
			}
			else
			{	// thing is A.Node having template override
				A.ChaldronTemplate.register(key, template.toTPL());
			}

			thing.all('ul').each(addTile, region); // initial item data
		},
		
		styleInvalidDropRegion = function(region)
		{
			var item = this.getData('item'),
				templates = region.getData('templates');

			if (!A.Array.find(templates, nameEquals, item.template))
			{	// invalid drop target
				region.transition({	duration: 0.6,
									opacity: { value: 0.4 }
									});
			}
		},
		
		startDrag = function(e)
		{	// style the drag node
			var tile = e.target.get('node'),
				drag = e.target.get('dragNode');
			
			drag.addClass('tile-single').html(tile.html());
			drag.setAttribute('class', tile.getAttribute('class'));
			drag.setStyle('opacity', '0.7');
			
			A.all(".TilesRegion").each(styleInvalidDropRegion, tile);
		},	

		beforeDragEvent = function(e)
		{	// swallow drag event if appropriate
			var item = e.target.get('node').getData('item'),
				dropNode = e.drop.get('node'),
				templates = dropNode.getData('templates');
			
			if (A.Lang.isUndefined(templates))
			{	// dropzone is a tile, not a region				
				templates = dropNode.get('parentNode').getData('templates');
	
				if (!A.Array.find(templates, nameEquals, item.template))
				{	// invalid drop target
					e.stopImmediatePropagation();
				}
			}
			else
			{	// dragging over region, not tile
				if(e.type === 'drag:over' || dropNode.all('.TilesTile').size() > 0)
				{
					e.stopImmediatePropagation();
				}
			}
		},
		
		dropTile = function(e)
		{	// drop tile onto empty region
			var region = e.drop.get('node'),
				tile = e.drag.get('node'),
				template = tile.getData('item').template,
				templates = region.getData('templates');
			
			if (region.all('.TilesTile').size() < 1)
			{	// region is empty, so drop can occur
				if (A.Array.find(templates, nameEquals, template))
				{	// drop is valid 
					region.insert(tile, 0);
					e.drop.sizeShim();
				}
			}
		},
		
		renderRegion = function(thing)
		{	// render all existing tiles in the targeted region
			var region = getRegion(thing),
				templates = region.getData('templates'),
				dropzone = new A.DD.Drop({node: region, groups: templates}),
				config = {container: region, nodes: '.TilesTile', groups: templates },
				sortable = new A.ChaldronSortable(config);
			
			sortable.before('drag:start', startDrag);
			sortable.before('drag:enter', beforeDragEvent);
			sortable.before('drag:over', beforeDragEvent);
			sortable.after('drag:end', fireTilesMoved);
				
			region.setData('sortable', sortable);
			region.setData('dropzone', dropzone);
			region.addClass('tile-single');
			region.delegate('click', fireTilesClick, '.TilesTile');
			dropzone.before('drop:hit', dropTile);
			
			region.all('.TilesTile').each(renderTile);
		},
	
		initRegion = function(thing)
		{	// remove all tiles/templates from region; get template mapping
			var region = emptyRegion(thing).setData('templates', []),
				mapping = getMapping(region);
			
			if (!A.Lang.isNull(mapping))
			{
				A.stamp(region);
				mapping.get('children').each(addTemplate, region);
				renderRegion(region);
			}
		};
		
		
		//////////////////////////////////////////////////////////////////////
		// Event Subscriptions API
		//
		// Control regions and tiles by publishing these events
		//
		
		Liferay.on(
			'TilesInitRegions',
			function(e)
			{	// Removes all tiles/templates from targeted region(s)
				if (A.Lang.isUndefined(e.regions))
				{
					A.Array.each(A.all('.TilesRegion'), initRegion);
				}
				else
				{
					A.Array.each(new A.Array(e.regions), initRegion);
				}
			}
		);
		
		Liferay.on(
			'TilesEmptyRegions',
			function(e)
			{	// Removes all tiles from the targeted region(s)				
				if (A.Lang.isUndefined(e.regions))
				{
					A.Array.each(A.all('.TilesRegion'), emptyRegion);
				}
				else
				{
					A.Array.each(new A.Array(e.regions), emptyRegion);
				}
			}
		);

		Liferay.on(
			'TilesAddTemplates',
			function(e)
			{	// Adds the template(s) to the targeted region
				A.Array.each(new A.Array(e.templates), addTemplate, getRegion(e));
			}
		);

		Liferay.on(
			'TilesAddItems',
			function(e)
			{	// adds the given items to the targeted region
				var items = new A.Array(e.items);
				
				if (!A.Lang.isUndefined(e.template))
				{	// if e.template defined, then apply it to all items
					A.Array.each(items, function(item) { item.template = e.template; }, e);
				}
				
				A.Array.each(new A.Array(e.items), addTile, getRegion(e));
			}
		);
	
		Liferay.on(
			'TilesRenderRegion',
			function(e)
			{	// render existing items
				if (A.Lang.isUndefined(e.items))
				{
					renderRegion(getRegion(e));
				}
			}
		);
		
		// Scan DOM, find and initialize regions, templates, and items
		A.all('.TilesRegion').each(initRegion);
			
		// Signal clients that this framework is ready to use
		Liferay.fire('TilesReady');
	}
);
