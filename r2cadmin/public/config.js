var config_module = angular.module('r2cAdminApp.config', [])
    .constant('APP_NAME','R2C Admin')
    .constant('APP_VERSION','0.1')
    .constant('R2CADMIN_CUSTOMERLIST_REST_URL', 'http://localhost:8083/r2crxmonitoring.json/rx/admin')
    .constant('R2CADMIN_MODIFIEDCUST_REST_URL', 'http://localhost:8083/r2crxmonitoring.json/rx/admin/modifiedRxCustomers')
;