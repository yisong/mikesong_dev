$().ready(function after_ready (argument) {

	$("#alert-popover").click(function click_event (argument) {
		console.log("click");

		var el = $("#alert-popover");

		var header = "Alert popover";
		var description = "something";

        if (!el.data('model'))
        {
            el.data('model', { 
                header: header, 
                description: description
            });
            el.data('model-template', "/chaldron-theme/templates/alertPopover.html");
        }

        C.PopoverManager.show(el)
        .then(
            function reposition_popover () {
                offset = el.offset();
                popover = el.next();
                popover.offset({ top: offset.top, left: offset.left - popover.width()});
        });;


	});

});
