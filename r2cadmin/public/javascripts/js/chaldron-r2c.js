
//////////////////////////////////////////////////////////////////////////////
// Chaldron.r2c Module definition
//

(	function($, me)
	{
		var r2cName = 'r2c';
		var r2c = me.ns(r2cName); // plug in to Chaldron namespace
		var tableSelector = { host: '[data-r2c-host-table]', volume: '[data-r2c-volume-table]' };
		var tilesSelector = { host: '[data-r2c-host-tiles]', volume: '[data-r2c-volume-tiles]' };
		var splitTableSelector = '[data-r2c-host-table-1]';
		var tableRowPath = { host: C.Assets.templatesPath + 'r2c/hostTableRow.html', volume: C.Assets.templatesPath + 'r2c/volumeTableRow.html' };
		var tilePath = { host: C.Assets.templatesPath + 'r2c/hostTile.html', volume: C.Assets.templatesPath + 'r2c/volumeTile.html' };
		var serverHealthPath = C.Assets.templatesPath + 'r2c/server-health.html';
		var totalServersPath = C.Assets.templatesPath + 'r2c/total-servers.html';
		var model = {};
		var customProtectedHostsData = new Array();
		var ready = new $.Deferred();
		
		var init = function(url)
		{	// initialize the r2c module
			var prefetch = $.when // prefetch artifacts in parallel
			(	C.Assets.fetch(url), // prefetch data 
				C.Assets.render(C.r2c.tableRowPath.host), // prefetch template
				C.Assets.render(C.r2c.tableRowPath.volume), // prefetch template
				C.Assets.render(C.r2c.serverHealthPath), // prefetch template
				C.Assets.render(C.r2c.totalServersPath), // prefetch template
				C.ready // and all imports done
			);

			C.say.loading(prefetch, 'The Recover To Cloud Application is initializing...');
	        $("#about-toggle").data('model', {});
	        $("#about-toggle").data('model-template', C.Assets.templatesPath + "r2c/r2c-about.html");
			$('#about-toggle').show();
			
			if (C.videoDrawer)
			{
				C.videoDrawer.addVideo('r2c-video-0', '//player.vimeo.com/video/80102274', 'Introduction to the Dashboard');
				C.videoDrawer.addVideo('r2c-video-1', '//player.vimeo.com/video/80306402', 'The Health Status Panel');
				C.videoDrawer.addVideo('r2c-video-2', '//player.vimeo.com/video/80410660', 'The RPO Panel');
				C.videoDrawer.addVideo('r2c-video-3', '//player.vimeo.com/video/80822666', 'The Storage Panel');
				C.videoDrawer.addVideo('r2c-video-4', '//player.vimeo.com/video/80924916', 'The Bandwidth Usage Panel');
				C.videoDrawer.addVideo('r2c-video-5', '//player.vimeo.com/video/82102279', 'Creating a Service Request');
			}
				
			prefetch.then
			(	function(data, status, xhr)
				{
					model = data[0];

					//getServerState(model.protectedHosts);
					
					if(model.protectedHosts.length < 1){
						$('#r2cMainContainer').hide();
						var msg = {};
						msg.responseText = "There is no server replication data available. Please contact customer support.";
						C.say.oops(msg, "No Data");
					}

					C.fadeInWebapp('#r2c-main');
					ready.resolve();
				}
			);
		}
		
		var clickExpandPaneClose = function()
		{
			var moveRowsUp = function()
			{	// merges two tables into one if expanded panel is not visible
				var lowerRows = $(splitTableSelector + ' .table-row');
		
				for (var r = 0; r < lowerRows.length; r++)
					$(tableSelector.host).append($(lowerRows[r]));

				C.r2c.backboneApp.setSelectedHost(-1, 'Host Table');
			}

			$(tableSelector['host'] + ' > *:last-child .squareIcon').addClass('invisible');
			$('.expandVolumePanel').slideUp("fast", moveRowsUp);				
		}
		
		var clickExpandPaneOpen = function(id)
		{
			if (id == C.r2c.backboneApp.getSelectedHost())
			{	// expand pane already open, so close it
				return clickExpandPaneClose();
			}

			var publishEvent = function()
			{
				C.r2c.backboneApp.setSelectedHost(id, 'Host Table');
			}
			
			openExpandPane(id);
			C.wait(250).then(publishEvent);
		}
		
		var openExpandPane = function(id)
		{			
			$('.expandVolumePanel').hide();
			$(tableSelector['host'] + ' > *:last-child .squareIcon').addClass('invisible');
			id = id === undefined? C.r2c.backboneApp.getSelectedHost() : id;

			var renderVolumes = function()
			{
				var upper = tableSelector.host; // top table
				var lower = splitTableSelector; // bottom table
				var columns = $(upper + ' > .table-head > .table-column');
				var upperRows = $(upper + ' .table-row');
				var lowerRows = $(lower + ' .table-row');
				var isUpper = upperRows.find('[data-id=' + id + ']').length > 0;
				
				// Sorted index represent the host order on current graph(always go from 0~N)
				var sortedIndex = 0;
				for(var iHost = 0; iHost < customProtectedHostsData.length; iHost++) {
					if(customProtectedHostsData[iHost]["originIndex"] === id) {
						sortedIndex = iHost;
						break;
					}
				}

				for (var r = upperRows.length - 1; isUpper && r > sortedIndex; r--)
					$(lower).prepend($(upperRows[r])); // shovel rows down
	
				for (var r = 0; !isUpper && r <= sortedIndex - upperRows.length; r++)
					$(upper).append($(lowerRows[r])); // shovel rows up
				
				for (var c = 1; c <= columns.length; c++)
				{	// use header col widths to approx col widths of lower table
					var w = $(columns[c-1]).css('width');
					var column = ' .table-row .table-column:nth-child(' + c + ')';
					$(lower + column).css('width', w);
				}
				
				$(tableSelector['volume'] + ' > *').slice(1).remove();

				var host = model.protectedHosts[id];
				renderItems(host.protectedVolumes, 'volume');

				$('#r2c-volume-table-header-text').text('The status of ' + host.name + ' reflects the status of its least healthy volume.');

				var filterFn = function(index, css)
				{	// remove any sg-color-* classes
					return (css.match (/\bsg-color-\S+/g) || []).join(' ');
				}
				
				$(".expandVolumePanel > div:first-child").removeClass(filterFn);	
				var status = host.rpoStatus == 'fail'? 'poor' : host.rpoStatus;
				$('.expandVolumePanel > div:first-child').addClass('sg-color-' + status);
				$('.expandVolumePanel').slideDown("fast");
				$(tableSelector['host'] + ' > *:last-child .squareIcon').removeClass('invisible');

			}
			
			if (id >= 0)
				C.yield().then(renderVolumes);
		}
		
		var renderItems = function(data, type)
		{
			var evenOdd = 'even';
			
			var appendTableRow = function(markup)
			{	// put markup into DOM as table row
				$(tableSelector[type]).append(markup);
				$(tableSelector[type] + ' [data-toggle]').tooltip({'delay': { show: 1000 } });
			}

			var renderItem = function(idx, item)
			{
				item.idx = type === "host" ? item["originIndex"] : idx;
				item.evenOdd = (evenOdd = evenOdd == 'even'? 'odd' : 'even');
				C.Assets.render(tableRowPath[type], item).then(appendTableRow);
			}

			$.each(data, renderItem);
		}

		var clickHealthTab = function()
		{
			if ($(tableSelector['host'] + ':visible').length < 1)
				updateHealthTab();
		}

		var refreshHealthTab = function()
		{
			if ($(tableSelector['host'] + ':visible').length > 0)
				updateHealthTab();
		}
		
		var updateHealthTab = function()
		{
			$('.expandVolumePanel').hide();
			$(splitTableSelector + ' > *').remove();
			$(tableSelector['host'] + ' > *').slice(1).remove();

			var scrollIntoView = function()
			{
				var x = $(tableSelector['host']).children().length - 4;
				
				if (x > 0)
				{
					var el = tableSelector['host'] + ' > *:nth-child(' + x + ')';
					$(el)[0].scrollIntoView();
				}
				else
					$(document).scrollTop(0);
			}

			var render = function()
			{

				if(C.r2c.backboneApp.getSelectedHost() >= 0) {
					// Place the selected host on top of the list
					var copy = $.extend(true, {}, model.protectedHosts);
					customProtectedHostsData = $.map(copy, function(val, key) { return [val]; });
					for(var iHost = 0; iHost < customProtectedHostsData.length; iHost++) {
						customProtectedHostsData[iHost]["originIndex"] = iHost;
					}

					var selectedIdx = C.r2c.backboneApp.getSelectedHost();
					var selectedHos = customProtectedHostsData[selectedIdx];

					customProtectedHostsData.splice(selectedIdx, 1);
					customProtectedHostsData.splice(0, 0, selectedHos);			
				}
				else {
					var copy = $.extend(true, {}, model.protectedHosts);
					customProtectedHostsData = $.map(copy, function(val, key) { return [val]; });
					for(var iHost = 0; iHost < customProtectedHostsData.length; iHost++) {
						customProtectedHostsData[iHost]["originIndex"] = iHost;
					}		
				}

				// renderItems(model.protectedHosts, 'host');
				renderItems(customProtectedHostsData, 'host');

				if (C.r2c.backboneApp.getSelectedHost() >= 0)
				{
					openExpandPane(C.r2c.backboneApp.getSelectedHost());
					C.wait(500).then(scrollIntoView);
				}

				var date = new Date();
				$("#tabel-date").text(C.r2c.backboneApp.getMonth(date.getMonth())+" "+date.getDate()+" "+date.getFullYear());
			}

			C.yield().then(render);
		}
	
        var Json2Csv = function(jsonData, csvTitle, showHeader) {

			function jsonHost2CSV(jsonHostData) {
				var hostrow = "";

				hostrow += jsonHostData.rpoStatus;
				hostrow += ",";
				hostrow += C.r2c.backboneApp.formatRPO(jsonHostData.currentRpo);
				hostrow += ",";
				hostrow += jsonHostData.name + "(" + jsonHostData.ip + ")";
				hostrow += ",";
				hostrow += jsonHostData.os;
				hostrow += ",";
				hostrow += jsonHostData.protectedVolumes.length;

				var jsonVolData = jsonHostData.protectedVolumes;
				for (var i = 0; i < jsonVolData.length; i++) {
					if (i > 0) {
						hostrow += ",,,,";
					}
					hostrow += ",";
					hostrow += jsonVolData[i].rpoStatus;
					hostrow += ",";
					hostrow += jsonVolData[i].name;
					hostrow += ",";
					hostrow += jsonVolData[i].protectionStatus;
					hostrow += ",";
					var protectedCapacity = jsonVolData[i].protectedCapacity;
					hostrow += C.r2c.backboneApp.formatStorage(protectedCapacity? protectedCapacity.value : 0, protectedCapacity? protectedCapacity.capacityUnit : 'GB');
					hostrow += ",";
					hostrow += C.r2c.backboneApp.formatRPO(jsonVolData[i].rpo);
					hostrow += ",";
					hostrow += C.r2c.backboneApp.formatRPO(jsonVolData[i].rpoThreshold);
					hostrow += "\r\n";
				}

				return hostrow;

			}

        	var csvHeaders = ['STATUS', 'RPO', 'HOST NAME(IP ADDRESS)', 'OS', 'VOLUMES', 'VOLUME STATUS', 'VOLUME NAME', 'STATE', 'SIZE', 'RPO', 'RPO THRESHOLD'];   
	        var CSV = '';    
	        CSV += csvTitle + '\r\n\n';

	        if (showHeader) {
	            var row = "";
	            for (var i = 0; i < csvHeaders.length; i++) {
	                row += csvHeaders[i] + ',';
	            }
	            row = row.slice(0, -1);
	            CSV += row + '\r\n';
	        }

	        for (var i = 0; i < jsonData.length; i++) {
	            var row = "";
				row += jsonHost2CSV(jsonData[i]);
	            row.slice(0, row.length - 1);
	            CSV += row + '\r\n';
	        }
	        
	        return CSV;
	    }

	    var exportCSV = function(csvData) {

			 function isIE () {
				 var myNav = navigator.userAgent.toLowerCase();
				 return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
			 }

		      // var uri = "/r2crxmonitoring.json/csvdownload";
		      // if (typeof(Chaldron) !== "undefined" && Chaldron.locateResource) {

		      //     // Prepare param object
		      //     var params = new Object();
		      //     params.verb = 'GET';
		      //     params.platformName = platform;
		      //     params.testPlanID = testPlanId;

		      //     uri = Chaldron.locateResource(uri, "#rstp-main", params);
		      // } 
		     //for Browser IE 10 and less, use server side download. Otherwise, using client side or server side download depends on the site 
		     //custom field ("OffLine"). Offline: true (using client side), otherwise using server side.
			 if ((isIE() && isIE () < 10) || !C.r2c.OffLine) {
				var csvdownloadUrl = C.locateResource("/r2crxmonitoring.json/csvdownload", "#r2c-main");
				window.location = csvdownloadUrl;
			 }
			 else {
		        var _filename = "hostreport_" + Math.round((new Date()).getTime() / 1000) + ".csv";
		        //IE10 and above is working differently than the rest of the browsers in terms of handling data URI
		        if(window.navigator.msSaveOrOpenBlob) {
		            var fileData = [csvData];
		            blobObject = new Blob(fileData);
		            window.navigator.msSaveOrOpenBlob(blobObject, _filename);
		        } else {
		            var uri = "data:text/csv;charset=utf-8," + escape(csvData);

		            var link = document.createElement("a");    
		            link.href = uri;
		            
		            //set the visibility hidden so it will not effect on your web-layout
		            link.style = "visibility:hidden";
		            link.download = _filename;
		            
		            //this part will append the anchor tag and remove it after automatic click
		            document.body.appendChild(link);
		            link.click();
		            document.body.removeChild(link);
		        }    
			 }

    
	    }

		var clickDownloadDoc = function()
		{
	        //convert the protected host data to the CSV format.
	        var csvData = Json2Csv(model.protectedHosts, "Proctected Host Data of " + model.name + " (as of " + (new Date()).toString() + ")", true);

	        if (csvData == '') {        
	            C.say.oops({"responseText" : "Invalid Protected Host Data."}, "Data Conversion Error");
	            return;
	        }   

	        //export the csv data as a file.
	        exportCSV(csvData);
		}

		// public interface
		r2c.init = init;
		r2c.getModel = function() { return model; };
		r2c.clickHealthTab = clickHealthTab;
		r2c.clickExpandPaneOpen = clickExpandPaneOpen;
		r2c.clickExpandPaneClose = clickExpandPaneClose;
		r2c.clickDownloadDoc = clickDownloadDoc;
		r2c.tableRowPath = tableRowPath;
		r2c.tilePath = tilePath;
		r2c.serverHealthPath = serverHealthPath;
		r2c.totalServersPath = totalServersPath;
		r2c.updateHealthTab = updateHealthTab;
		r2c.refreshHealthTab = refreshHealthTab;
		r2c.ready = ready;
	}
)(jQuery, C);
