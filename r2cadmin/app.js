
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3333);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('stylus').middleware(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// application -------------------------------------------------------------
app.get('/r2cadmincusts', function(req, res) {
	res.sendfile('./public/r2cadmincusts.html'); // load the single view file (angular will handle the page changes on the front-end)
});

app.get('/r2cadminmodcusts', function(req, res) {
	res.sendfile('./public/r2cadminmodcusts.html'); // load the single view file (angular will handle the page changes on the front-end)
});

app.get('/downloadCSV', function(req, res) {
	res.setHeader('Content-disposition', 'attachment; filename=theDocument.txt');
	res.setHeader('Content-type', 'text/csv');
	res.charset = 'UTF-8';
	res.write("Hello, world");
	res.end();
});



// app.get('/download', function(req, res){
//   var file = __dirname + '/upload-folder/dramaticpenguin.MOV';
//   res.download(file); // Set disposition and send it.

//   res.setHeader('Content-disposition', 'attachment; filename=testing.csv');
//     res.writeHead(200, {
//         'Content-Type': 'text/csv'
//     });

//     csv().from(data).to(res)

// });

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
