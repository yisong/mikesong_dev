
var r2cAdmin = angular.module('r2cAdmin',['ngResource', 'r2cAdminApp.config']);
  
r2cAdmin.controller('CustomerController', ['$scope', '$resource', 'R2CADMIN_CUSTOMERLIST_REST_URL', 'R2CADMIN_MODIFIEDCUST_REST_URL', function($scope, $resource, R2CADMIN_CUSTOMERLIST_REST_URL, R2CADMIN_MODIFIEDCUST_REST_URL) {
 
    // initialize REST API access by creating Customers class
    $scope.Customers = $resource(R2CADMIN_CUSTOMERLIST_REST_URL);
    $scope.ModifiedCustomers = $resource(R2CADMIN_MODIFIEDCUST_REST_URL);
 
    var date = new Date();
    $scope.curDate = date.getMonth()+"/"+date.getDate()+"/"+date.getFullYear();

 	  $scope.ListCustomers = function() {
      // call static query() method of Customer class
      $scope.Customers.query(function (data) {
        //data format conversion
        _.map(data, function(dt) { dt.rpoinMins = (dt.latestMaxRpoAllHosts != null) ? dt.latestMaxRpoAllHosts.hr * 60 + dt.latestMaxRpoAllHosts.min : -1; 
                                   return dt; });
        _.map(data, function(dt) { dt.latestMaxRpoAllHosts = (dt.latestMaxRpoAllHosts != null) ? $scope.formatRPO(dt.latestMaxRpoAllHosts) : 'n/a'; return dt; });
        _.map(data, function(dt) { dt.latesttMinRpoAllHosts = (dt.latesttMinRpoAllHosts != null) ? $scope.formatRPO(dt.latesttMinRpoAllHosts) : 'n/a'; return dt; });
        _.map(data, function(dt) { dt.dataCollectionDate = (dt.dataCollectionDate != null) ? $scope.showLocalDate(dt.dataCollectionDate) : 'n/a'; return dt; });


        $scope.customers = data;
        console.log("Customer.data.length=" + $scope.customers.length + ", name=" + $scope.customers[0].name);
      }, function(err) {
        console.log("Customer.query() error : "+err);
      });
    };

    $scope.ListModCustomers = function() {
        // call static query() method of Modified Customer class
        $scope.ModifiedCustomers.query(function (data) {
          _.map(data, function(dt) { dt.changeTime = (dt.changeTime != null) ? $scope.showLocalDate(dt.changeTime) : 'n/a'; return dt; });
          $scope.modcustomers = data;
          console.log("Modified Customer.data.length=" + $scope.modcustomers.length + ", name=" + $scope.modcustomers[0].name);
      }, function(err) {
        console.log("Customer.query() error : "+err);
      });
    };

    var mmToMonth = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

    $scope.showLocalDate = function(timestamp)
    {
      var dt = new Date(timestamp);
      var mm = mmToMonth[dt.getMonth()];
      return dt.getDate() + "-" + mm + "-" + dt.getFullYear();
    };

    $scope.formatRPO = function(rpoData, format) {
      var d,h,m,s;
      var ret = "";
      if(rpoData['hr'] > 24) {
        d = Math.floor(rpoData['hr'] / 24);
        h = rpoData['hr'] - (d * 24);
      } else if(rpoData['hr'] > 0) {
        h = rpoData['hr'];
        m = rpoData['min'];
      } else {
        m = rpoData['min'];
        s = rpoData['sec'];
      }
      if(d) {
        ret = ret + d;
        ret = format == "long" ? ret + " days " : ret + "d ";
      }
      if(h) {
        ret = ret + h;
        ret = format == "long" ? ret + " hours " : ret + "h ";
      }
      if(m) {
        ret = ret + m;
        ret = format == "long" ? ret + " mins " : ret + "m ";
      }
      if(s) {
        ret = ret + s;
        ret = format == "long" ? ret + " secs " : ret + "s ";
      }
      return ret == "" ? format == "long" ? "0 secs" : "0s" : ret.substring(0, ret.length-1);
    };

    $scope.convertToCSV = function(_data, showHeader) {
      var csvTitle = "Customer Health List" + " (" + (new Date()).toString() + ")";
      var csvHeaders = ['Customer Name', 'Oracle ID', 'Rx ID', 'Number of Protected Servers', 'MAX RPO', 'MIN RPO', 'Data Collection Date']; 
      var CSV = '';    
      CSV += csvTitle + '\r\n\n';

      if (showHeader) {
          var row = "";
          for (var i = 0; i < csvHeaders.length; i++) {
              row += csvHeaders[i] + ',';
          }
          row = row.slice(0, -1);
          CSV += row + '\r\n';
      }

      //Process the data
      angular.forEach(_data, function (row, index) {
          var infoArray = [];
          angular.forEach(row, function (field) {
            if (typeof field === 'string' && $scope.txtDelim) {
                field = $scope.txtDelim + field + $scope.txtDelim;
            }
            this.push(field);
          }, infoArray);
          dataString = infoArray.join(',');
          CSV += index < _data.length ? dataString + '\r\n' : dataString;
      });

      return CSV;
    };

    $scope.exportCSV = function(csvData) {
        var _filename = "hostreport_" + Math.round((new Date()).getTime() / 1000) + ".csv";
        //IE10 and above is working differently than the rest of the browsers in terms of handling data URI
        if(window.navigator.msSaveOrOpenBlob) {
            var fileData = [csvData];
            blobObject = new Blob(fileData);
            window.navigator.msSaveOrOpenBlob(blobObject, _filename);
        } else {
            var uri = "data:text/csv;charset=utf-8," + escape(csvData);

            var link = document.createElement("a");    
            link.href = uri;
                
            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = _filename;
                
            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }   
    };

    $scope.csvDownloadClick = function() {
      var _convdt = $scope.convertToCSV($scope.customers, true);
      $scope.exportCSV(_convdt);
    };

    $scope.orderProp = 'name';

    // first, query all customers to intialize $scope.customers
    $scope.ListCustomers();
    $scope.ListModCustomers();
}]);


